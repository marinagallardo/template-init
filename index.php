<?php @include "header.php" ?>

<div role="main"> 

    <div class="section-content">
        <div class="content-wrapper">

            <section class="section-left">
                <header class="first-page-header">
                    <h2>Estado General</h2>
                    <h3>Alcalá de Guadaira</h3>
                </header>

                <article>
                    <header class="second-page-header">
                        <h3>Desglose de participación</h3>
                        <p>Última actualización: <span data-update="13:33">13:33</span></p>
                    </header>
                    <table>
                        <thead>
                            <tr>
                                <th>Grupo</th>
                                <th>Censo</th>
                                <th>Participación</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Militantes</td>
                                <td>7</td>
                                <td><span>3</span> 42,86%</td>
                            </tr>
                            <tr>
                                <td>Militantes</td>
                                <td>7</td>
                                <td><span>3</span> 42,86%</td>
                            </tr>
                            <tr>
                                <td>Militantes</td>
                                <td>7</td>
                                <td><span>3</span> 42,86%</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td>668</td>
                                <td><span>3</span> 42,86%</td>
                            </tr>
                        </tfoot>
                    </table>
                </article>

                <article>
                    <header class="third-page-header">
                        <h4>Participación actual</h4>
                        <p><span data-votantes="262">262</span> Votantes</p>
                    </header>
                    Gráfica de progreso
                </article>

                <article>
                    <h4>Municipales 2011</h4>
                    Gráfica de progreso
                </article>

            </section>

            <section class="section-right">
                <h3>Evolución de la participación</h3>
                Pie Graphics
            </section> 
        </div>
    </div>

    <nav class="section-menu">
        <ul>
            <li class="active">
                <a href="#">item</a>
            </li>
            <li>
                <a href="#">item</a>
            </li>
            <li>
                <a href="#">item</a>
            </li>
        </ul>
    </nav>

</div>

<?php @include "footer.php" ?>
'use strict';

// npm install time-grunt -g
// npm install load-grunt-tasks -g
// npm install grunt-contrib-watch -g
// npm install grunt-contrib-sass -g

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    grunt.initConfig({
        compass: {
            dist: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'stylesheets',
                    environment: 'production'
                }
            },
            dev: {
                options: {
                    sassDir: 'sass',
                    cssDir: 'stylesheets'
                }
            }
        },  

        concat: {
            options: {
              separator: ';',
            },
            dist: {
              src: ['js/{,**/}*.js', '!js/main.min.js', '!js/jquery-1.11.2.min.js', '!js/main.js'],
              dest: 'js/main.js'
            },
        },        

        uglify: {
            my_target: {
              files: {
                'js/main.min.js': ['js/main.js']
              }
            }
        },              

        watch: {
            sass: {
                files: ['sass/{,**/}*.{sass,scss}'],
                tasks: ['compass:dev']
            },

            js: {
                files: ['js/{,**/}*.js', '!js/main.min.js', '!js/jquery-1.11.2.min.js'],
                tasks: ['concat', 'uglify']
            },

            options: {
              livereload: true
            }
        }
    });

    grunt.registerTask('default', ['compass:dev', 'concat', 'uglify', 'watch']);

};
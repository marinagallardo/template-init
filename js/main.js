$(function(){
    
    $('.section-menu li').click(function(){
        
        var arrayTabs = $('.section-menu li'); //te guardas el array de tabs (li)
        var tabClicked = $(this); //te guardas sobre el que has clicado
        var tabClickedIndex = tabClicked.index(); // y su indice
        tabClicked.css('z-index', arrayTabs.length - 1); //le pones el z-indez mayor del total
        var zindex = 0; //pones el zindex a 0
        arrayTabs.each(function(index){ //recorres todos ponienzo zindex en orden menos al clicado
            $(this).removeClass('active'); //quitas la clase selected a todos
            if($(this).index() != tabClickedIndex){
                  $(this).css('z-index', zindex);
                   zindex += 1; //incrementas el zindex
            }                
        });       
        tabClicked.addClass('active'); //pones la clase selected al clicado
    });   
    
});